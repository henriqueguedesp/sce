<?php

namespace SCE\Models;

use SCE\Util\Conexao;
use PDO;
use SCE\Entity\Apontamento;
use SCE\Entity\ApontamentoGenerico;

class ModeloApontamento
{

    function __construct()
    {
    }

    public function relatorioPaletePorExpedicao($codigoExpedicao)
    {
        try {

            //$sql = "select * from apontamento where  status = 1 and saldo > 0";
            //    $sql = "select ordemExpedicao,codigoPalete from expedicao;";
            $sql = 'SELECT e.ordemExpedicao, e.codigoPalete, a.saldo 
                    FROM expedicao AS e, apontamento AS a
                    WHERE e.ordemExpedicao = :codigoExpedicao and e.codigoPalete  = a.codigoPalete';
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigoExpedicao', $codigoExpedicao);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    public function reativar($codigo, $idUsuario)
    {
        try {
            $conexao = Conexao::getInstance();

            $status = "update apontamento as a, enderecamento as e set e.status = 1, a.status = 1 "
                . " where e.idApontamento = a.idApontamento and a.codigoPalete = :palete;";
            $conexao->beginTransaction();
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':palete', $codigo);
            $p_status->execute();

            $sql = "select * from apontamento where  codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);
            $p_sql->execute();
            $codigo = $p_sql->fetch(PDO::FETCH_OBJ);



            $status = "insert into historicoReativamento (idApontamento, idUsuario, data)"
                . " values "
                . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':idApontamento', $codigo->idApontamento);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }

    public function editarApontamentoGenerico($codigoGenerico, $idGenerico, $idUsuario, $codigoAntigo)
    {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "update apontamentoGenerico set codigoPalete = :palete where "
                . " codigoPalete = :codigoAntigo";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $codigoGenerico);
            $p_sql->bindValue(':codigoAntigo', $codigoAntigo);
            $p_sql->execute();

            $status = "insert into historicoReapontamento(idApontamento, idUsuario, tipo, data, codigoPalete, saldo,  "
                . "tipoPalete )"
                . " values "
                . "(:idApontamento, :idUsuario,0,now(), :codigoPalete, -1000, -2)";

            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $idGenerico);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':codigoPalete', $codigoAntigo);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }

    public function editarApontamento(Apontamento $apontamento, $idUsuario, $apontamentoAntigo)
    {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "update apontamento set codigoPalete = :palete, tipo = :tipo, saldo = :saldo where "
                . " idApontamento = :id";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':palete', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':id', $apontamento->getIdApontamento());
            $p_sql->execute();

            $status = "insert into historicoReapontamento(idApontamento, idUsuario, tipo, data, codigoPalete, saldo,  "
                . "tipoPalete )"
                . " values "
                . "(:idApontamento, :idUsuario,1,now(), :codigoPalete, :saldo, :tipoPalete)";

            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $apontamento->getIdApontamento());
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':codigoPalete', $apontamentoAntigo->codigoPalete);
            $p_status->bindValue(':saldo', $apontamentoAntigo->saldo);
            $p_status->bindValue(':tipoPalete', $apontamentoAntigo->tipo);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }

    public function apontarGenericoGenerico(ApontamentoGenerico $apontamento, $idUsuario, $palete)
    {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //$sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, saldo, status) "
                . " values "
                //. " (:cod, :origem, :tipo, :saldo, :status)";
                . " (:cod, :origem, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':origem', $apontamento->getPaleteOrigem());
            //$p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $saldo = "update apontamentoGenerico set saldo = saldo - :quant where codigoPalete = :palete and paleteOrigem = :origem";
            $p_saldo = $conexao->prepare($saldo);
            $p_saldo->bindValue(':quant', $apontamento->getSaldo());
            $p_saldo->bindValue(':palete', $palete);
            $p_saldo->bindValue(':origem', $apontamento->getPaleteOrigem());
            $p_saldo->execute();


            $veri = "update apontamentoGenerico set status = 0 where saldo = 0";
            $p_veri = $conexao->prepare($veri);

            $p_veri->execute();

            $status = "insert into historicoApontamentoGenerico (idApontamentoGenerico, idUsuario, data)"
                . " values "
                . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
        }
    }

    public function materialPaleteGenerico($codigo)
    {
        try {

            $sql = "select * from apontamentoGenerico where  status = 1 and codigoPalete = :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    public function verificaPaleteGenerico($codigo)
    {
        try {

            $sql = "select * from apontamentoGenerico where codigoPalete = :codigo and status = 1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    public function apontarGenerico(ApontamentoGenerico $apontamento, $idUsuario)
    {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();
            //            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem, tipo, saldo, status)"
            $sql = "insert into apontamentoGenerico (codigoPalete, paleteOrigem,  saldo, status)"
                . " values"
                //. " (:cod, :origem, :tipo, :saldo, :status)";
                . " (:cod, :origem, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigoPalete());
            $p_sql->bindValue(':origem', $apontamento->getPaleteOrigem());
            //$p_sql->bindValue(':tipo', $apontamento->getTipo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $saldo = "update apontamento set saldo = saldo - :quant where codigoPalete = :palete";
            $p_saldo = $conexao->prepare($saldo);
            $p_saldo->bindValue(':quant', $apontamento->getSaldo());
            $p_saldo->bindValue(':palete', $apontamento->getPaleteOrigem());
            $p_saldo->execute();


            $veri = "update apontamento set status = 0 where saldo = 0";
            $p_veri = $conexao->prepare($veri);

            $p_veri->execute();

            $status = "insert into historicoApontamentoGenerico (idApontamentoGenerico, idUsuario, data)"
                . " values "
                . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
        }
    }

    public function verificaPaleteEnderecado($codigo)
    {
        try {

            $sql = "select * from apontamento as a, enderecamento as e where a.idApontamento= e.idApontamento and a.codigoPalete = :codigo and a.status = 1 and a.saldo > 0";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    public function verificaPaleteExpedicao($codigo)
    {
        //metodo que irá verificar se o palete já foi expedido
        try {

            $sql = "select * from apontamento as a, expedicao as e where a.codigoPalete = e.codigoPalete and a.codigoPalete =  :codigo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    //Rotina de apontamento de materiais
    public function apontar(Apontamento $apontamento, $idUsuario)
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = "insert into apontamento (codigo,  saldo, status)"
                . " values"
                . " (:cod, :saldo, :status)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':cod', $apontamento->getCodigo());
            $p_sql->bindValue(':saldo', $apontamento->getSaldo());
            $p_sql->bindValue(':status', $apontamento->getStatus());
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $status = "insert into historicoApontamento (idApontamento, idUsuario, data)"
                . " values "
                . "(:idApontamento, :idUsuario,now())";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idApontamento', $id);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->execute();

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }
    public function realizarRetorno($idS, $valores, $idUsuario)
    {
        try {
            $conexao = Conexao::getInstance();
            $conexao->beginTransaction();

            foreach ($idS as $key => $dado) {
                $sql = 'UPDATE apontamento SET   status = 1, saldo  = :saldo
                    WHERE 
                    idApontamento  = :id ';

                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':id', $dado);
                $p_sql->bindValue(':saldo', $valores[$key]);
                $p_sql->execute();

                $status = 'INSERT INTO historicoRetorno (idApontamento, idUsuario, data)
                       values 
                       (:idApontamento, :idUsuario,now())';
                $p_status = $conexao->prepare($status);
                $p_status->bindValue(':idApontamento', $dado);
                $p_status->bindValue(':idUsuario', $idUsuario);
                $p_status->execute();
            }

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }
    public function realizarExpedicao($dados, $idUsuario)
    {
        try {
            $conexao = Conexao::getInstance();
            $conexao->beginTransaction();

            foreach ($dados as $dado) {
                $sql = 'UPDATE apontamento SET   status = 2
                    WHERE 
                    idApontamento  = :id ';

                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(':id', $dado);
                $p_sql->execute();

                $status = 'INSERT INTO historicoExpedicao (idApontamento, idUsuario, date)
                       values 
                       (:idApontamento, :idUsuario,now())';
                $p_status = $conexao->prepare($status);
                $p_status->bindValue(':idApontamento', $dado);
                $p_status->bindValue(':idUsuario', $idUsuario);
                $p_status->execute();
            }

            $conexao->commit();
        } catch (Exception $ex) {
        }
    }


    //Rotina que retorna todos os materiais que podem ser expedidos
    public function apontamentosARetornaCodigo($codigo)
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = "SELECT * FROM apontamento AS a
                    LEFT JOIN historicoApontamento AS hA ON hA.idApontamento  = a.idApontamento
                    WHERE a.status = 2 AND a.saldo != 0 AND a.codigo LIKE  concat('%',:codigo,'%')";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetchALL(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }
    //Rotina que retorna todos os materiais que podem ser expedidos
    public function apontamentosAExpedirCodigo($codigo)
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = "SELECT * FROM apontamento AS a
                    LEFT JOIN historicoApontamento AS hA ON hA.idApontamento  = a.idApontamento
                    WHERE a.status = 1 AND a.saldo != 0 AND a.codigo LIKE  concat('%',:codigo,'%')";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':codigo', $codigo);

            $p_sql->execute();
            return $p_sql->fetchALL(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }
    //Rotina que retorna todos os materiais que podem ser expedidos
    public function apontamentosAExpedir()
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'SELECT * FROM apontamento AS a
                    LEFT JOIN historicoApontamento AS hA ON hA.idApontamento  = a.idApontamento
                    WHERE a.status = 1 AND a.saldo = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchALL(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }

    //Rotina que retorna todos os materiais que podem ser expedidos
    /*public function apontamentosAExpedir()
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'SELECT * FROM apontamento AS a
                    LEFT JOIN historicoApontamento AS hA ON hA.idApontamento  = a.idApontamento
                    WHERE a.status = 1 AND a.saldo = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchALL(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }*/

    //Rotina que retorna todos os materiais que devem ser retornados
    public function apontamentosARetornar()
    {
        try {
            $conexao = Conexao::getInstance();
            $sql = 'SELECT * FROM apontamento AS a
                    LEFT JOIN historicoExpedicao AS hE ON hE.idApontamento  = a.idApontamento
                    WHERE a.status = 2 AND a.saldo = 1;';
            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->execute();
            return $p_sql->fetchALL(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
        }
    }
}
