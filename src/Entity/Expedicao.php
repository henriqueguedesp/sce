<?php

namespace UBSValorem\Entity;

class Expedicao {

    private $idExpedicao;
    private $ordemExpedicao;
    private $codigoPalete;
    private $quantidade;

    function __construct() {
        
    }

    function getIdExpedicao() {
        return $this->idExpedicao;
    }

    function getOrdemExpedicao() {
        return $this->ordemExpedicao;
    }

    function getCodigoPalete() {
        return $this->codigoPalete;
    }

    function getQuantidade() {
        return $this->quantidade;
    }

    function setIdExpedicao($idExpedicao) {
        $this->idExpedicao = $idExpedicao;
    }

    function setOrdemExpedicao($ordemExpedicao) {
        $this->ordemExpedicao = $ordemExpedicao;
    }

    function setCodigoPalete($codigoPalete) {
        $this->codigoPalete = $codigoPalete;
    }

    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

}
