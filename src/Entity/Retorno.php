<?php



namespace UBSValorem\Entity;


class Retorno {
    
    private $idRetorno;
    private $idUsuario;
    private $idOrdem;
    private $idApontamento;
    private $data;
    
    function getIdRetorno() {
        return $this->idRetorno;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function getIdOrdem() {
        return $this->idOrdem;
    }

    function getIdApontamento() {
        return $this->idApontamento;
    }

    function getData() {
        return $this->data;
    }

    function setIdRetorno($idRetorno) {
        $this->idRetorno = $idRetorno;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    function setIdOrdem($idOrdem) {
        $this->idOrdem = $idOrdem;
    }

    function setIdApontamento($idApontamento) {
        $this->idApontamento = $idApontamento;
    }

    function setData($data) {
        $this->data = $data;
    }

    function __construct() {
        
    }

}
