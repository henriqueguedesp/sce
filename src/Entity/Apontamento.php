<?php

namespace SCE\Entity;

class Apontamento {

    private $idApontamento;
    private $codigo;
    private $saldo;
    private $status;

    function __construct() {
        
    }

    function getIdApontamento() {
        return $this->idApontamento;
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getSaldo() {
        return $this->saldo;
    }

    function getStatus() {
        return $this->status;
    }

    function setIdApontamento($idApontamento) {
        $this->idApontamento = $idApontamento;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setSaldo($saldo) {
        $this->saldo = $saldo;
    }

    function setStatus($status) {
        $this->status = $status;
    }

}
