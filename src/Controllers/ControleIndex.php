<?php

namespace SCE\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SCE\Util\Sessao;
use SCE\Models\ModeloSala;
use SCE\Models\ModeloAgendamento;
use SCE\Models\ModeloEmail;

class ControleIndex {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function erro404() {
        return $this->response->setContent($this->twig->render('Erro404.html.twig'));
    }

    public function dashboard() {
        $usuario = $this->sessao->get('usuarioSCE');
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Dashboard.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
