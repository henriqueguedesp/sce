<?php

namespace SCE\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use SCE\Util\Sessao;
use SCE\Models\ModeloApontamento;
use SCE\Entity\Apontamento;
use SCE\Entity\ApontamentoGenerico;

class ControleApontamento
{

    private $response;
    private $twig;
    private $request;
    private $sessao;

    public function reativar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $verifica = $modelo->verificaCodigoReativar($codigo);
            //if (!$verifica) {
            //    $verifica = $modelo->verificaCodigoGenerico($codigo);
            // }

            if ($codigo == null) {
                echo 2;
            } else {
                if ($verifica) {
                    //  echo 0;
                    // uncao de reativacao
                    $modelo->reativar($codigo, $usuario->idUsuario);
                    echo 0;
                } else {
                    echo 1;
                }
            }
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function reativamento()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ReativarApontamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function salvarReapontar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $quantidade = $this->request->get('quantidade');
            $id = $this->request->get('id');
            $tamanho = strlen($codigo);
            $ultimo = $codigo[$tamanho - 1];
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $apontamento = new Apontamento();

            $apontamento->setCodigoPalete($codigo);
            $apontamento->setIdApontamento($id);
            $apontamento->setSaldo($quantidade);

            $tipo = substr($dados[0], $tamanho - 2);
            if ($tipo == 'SC' || $tipo == 'BG') {
                if ($tipo == 'SC') {
                    $apontamento->setTipo(1);
                } else {
                    $apontamento->setTipo(0);
                }
                $modelo = new ModeloApontamento();
                $dados = $modelo->verificaId($apontamento->getIdApontamento());

                $modelo->editarApontamento($apontamento, $usuario->idUsuario, $dados);
                //print_r($apontamento);
            } else {
                echo 0;
            }

            $modelo = new ModeloApontamento();
            //$dados = $modelo->verificaCodigo($codigo);
            //print_r($dados);
            echo 0;
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function verificaCodigoReapontar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');

            if ($codigo == null) {
                echo 2;
            } else {

                $tamanho = strlen($codigo);
                $ultimo = $codigo[$tamanho - 1];
                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                [$tamanho - 1];
                $tipo = substr($dados[0], $tamanho - 2);
                if ($tipo == 'PG') {
                    if ($id) {
                        $verifica = $modelo->verificaCodigoGenericoId($codigo, $id);
                    } else {
                        $verifica = $modelo->verificaCodigoGenerico($codigo);
                    }

                    if ($verifica) {
                        echo 1;
                    } else {
                        $data = $modelo->verificaIdGenerico($id);
                        $modelo->editarApontamentoGenerico($codigo, $data->idApontamentoGenerico, $usuario->idUsuario, $data->codigoPalete);
                        //echo 0;
                        echo -2;
                    }
                } else {
                    if ($id) {
                        $verifica = $modelo->verificaCodigoComId($codigo, $id);
                    } else {
                        $verifica = $modelo->verificaCodigo($codigo);
                    }

                    if ($tipo == 'SC' || $tipo == 'BG') {
                        if ($verifica) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    } else {

                        echo 3;
                    }
                }
            }
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function reapontarFinal()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');
            $modelo = new ModeloApontamento();
            $data = $modelo->verificaId($id);

            $tamanho = strlen($codigo);
            $ultimo = $codigo[$tamanho - 1];
            $dados = explode('-', $codigo);
            $tamanho = strlen($dados[0]);
            $tipo = substr($dados[0], $tamanho - 2);

            return $this->response->setContent($this->twig->render('SalvarReapontar.html.twig', array('user' => $usuario, 'codigo' => $codigo, 'id' => $id, 'tipo' => $tipo, 'dado' => $data)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function reapontar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $codigo = $this->request->get('codigo');
            $modelo = new ModeloApontamento();
            $dados = $modelo->verificaCodigo($codigo);
            if (!$dados) {
                $dados = $modelo->verificaCodigoGenerico($codigo);
            }
            //print_r($dados);
            return $this->response->setContent($this->twig->render('Reapontar.html.twig', array('user' => $usuario, 'dado' => $dados)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function verificaCodigoEditar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $verifica = $modelo->verificaCodigo($codigo);
            if (!$verifica) {
                $verifica = $modelo->verificaCodigoGenerico($codigo);
            }
            if ($codigo == null) {
                echo 2;
            } else {
                if ($verifica) {
                    echo 0;
                } else {
                    echo 1;
                }
            }
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function editarApontamento()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            return $this->response->setContent($this->twig->render('ApontamentoEditar.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao)
    {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }



    public function confirmarRetorno()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $idS = explode(",", $this->request->get('valoresRetornar'));
            $valores = [];
            foreach ($idS as $id) {
                array_push($valores, $this->request->get('SLT' . $id));
            }
            // print_r($idS);
            // print_r("<br>");
            // print_r($valores);
            $modelo = new ModeloApontamento();
            $id = $modelo->realizarRetorno($idS, $valores, $usuario->idUsuario);
            echo 1;
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }
    public function confirmarExpedicao()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {


            $dados = explode(",", $this->request->get('dados'));
            $modelo = new ModeloApontamento();

            $id = $modelo->realizarExpedicao($dados, $usuario->idUsuario);

            ///$id = $modelo->apontar($apontamento, $usuario->idUsuario);
            echo 1;
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function realizarApontamento()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            sleep(1);
            $modelo = new ModeloApontamento();
            $apontamento = new Apontamento();
            $apontamento->setCodigo($this->request->get('codigo'));
            $apontamento->setSaldo(1);
            $apontamento->setStatus(1);
            $id = $modelo->apontar($apontamento, $usuario->idUsuario);
            echo 0;
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }


    public function realizarExpedicao($codigo)
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $apontamento = new Apontamento();
            $apontamento->setCodigo($codigo);
            $dados = $modelo->apontamentosAExpedirCodigo($apontamento->getCodigo());
            echo json_encode($dados);
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }
    public function realizarRetorno($codigo)
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $apontamento = new Apontamento();
            $apontamento->setCodigo($codigo);
            $dados = $modelo->apontamentosARetornaCodigo($apontamento->getCodigo());
            echo json_encode($dados);
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }



    public function verificaCodigo()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $codigo = $this->request->get('codigo');
            $id = $this->request->get('id');
            if ($id) {
                $verifica = $modelo->verificaCodigoComId($codigo, $id);
            } else {
                $verifica = $modelo->verificaCodigo($codigo);
            }
            if ($codigo == null) {
                echo 2;
            } else {
                $tamanho = strlen($codigo);
                $ultimo = $codigo[$tamanho - 1];
                $dados = explode('-', $codigo);
                $tamanho = strlen($dados[0]);
                $tipo = substr($dados[0], $tamanho - 2);
                if (($tipo == 'SC' && $ultimo == 'P')) {
                    if ($verifica) {
                        echo 1;
                    } else {
                        echo 0;
                    }
                } else {
                    if (($tipo == 'BG' && $ultimo == 'B')) {
                        if ($verifica) {
                            echo 1;
                        } else {
                            echo 0;
                        }
                    } else {
                        echo 3;
                    }
                }
            }
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function apontar()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            return $this->response->setContent($this->twig->render('Apontamento.html.twig', array('user' => $usuario)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }
    public function retorno()
    {
        $usuario = $this->sessao->get("usuarioSCE");
        if ($usuario) {
            $modelo = new ModeloApontamento();
            $apontamentos = $modelo->apontamentosARetornar();
            return $this->response->setContent($this->twig->render('Retorno.html.twig', array('user' => $usuario, 'apontamentos' => $apontamentos)));
        } else {
            $this->redireciona('/sce/public_html/login');
        }
    }

    public function redireciona($destino)
    {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }
}
