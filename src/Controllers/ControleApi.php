<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloEnderecamento;
use UBSValorem\Models\ModeloApontamento;
use UBSValorem\Entity\Enderecamento;
use SoapClient;
use SoapFault;

class ControleApi {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function apiApontamento() {
        $params = array('CODIGOBARRAS' => '000108000200003', 'PESO' => '100');
        $wsdl = 'http://172.16.22.201:8070/ws/WSPCP002.apw?WSDL';
        $options = array(
            'uri' => 'http://schemas.xmlsoap.org/soap/envelope/',
            'style' => SOAP_RPC,
            'use' => SOAP_ENCODED,
            'soap_version' => SOAP_1_1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'connection_timeout' => 15,
            'trace' => true,
            'encoding' => 'UTF-8',
            'exceptions' => true,
        );
        try {
            $soap = new SoapClient($wsdl, $options);
            $soap->WSMPCP002($params);
            echo 'ok';
        } catch (SoapFault $e) {
            echo $e->getMessage(); 
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
