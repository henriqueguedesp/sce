<?php

namespace SCE\Routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array('_controller' => 
    'SCE\Controllers\ControleIndex',
    '_method' => 'dashboard'))); 

##LOGIN
$rotas->add('login', new Route('/login  ', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'paginaLogin'))); 

$rotas->add('validaLogin', new Route('/validaLogin', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'validaLogin'))); 

$rotas->add('removerUsuario', new Route('/removerUsuario', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'removerUsuario'))); 

$rotas->add('controleUsuario', new Route('/controleUsuario', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'controleUsuario'))); 

$rotas->add('cadastrarUsuario', new Route('/cadastrarUsuario', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'cadastrarUsuario'))); 

$rotas->add('editarUsuario', new Route('/editarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'editarUsuario'))); 

$rotas->add('ativarUsuario', new Route('/ativarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'ativarUsuario'))); 

$rotas->add('desativarUsuario', new Route('/desativarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'desativarUsuario'))); 

$rotas->add('erro404', new Route('/erro404', array('_controller' => 
    'SCE\Controllers\ControleIndex',
    '_method' => 'erro404'))); 

##CONTROLE DE USUÁRIO
$rotas->add('controleUsuario', new Route('/controleUsuario', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'controleUsuario'))); 

$rotas->add('cadastrarUsuario', new Route('/cadastrarUsuario', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'cadastrarUsuario'))); 

$rotas->add('editarUsuario', new Route('/editarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'editarUsuario'))); 

$rotas->add('ativarUsuario', new Route('/ativarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'ativarUsuario'))); 

$rotas->add('desativarUsuario', new Route('/desativarUsuario/{_param}', array('_controller' => 
    'SCE\Controllers\ControleUsuario',
    '_method' => 'desativarUsuario'))); 

$rotas->add('erro404', new Route('/erro404', array('_controller' => 
    'SCE\Controllers\ControleIndex',
    '_method' => 'erro404'))); 

##CONTROLE PERFIL
$rotas->add('controlePerfil', new Route('/controlePerfil', array('_controller' => 
    'SCE\Controllers\ControlePerfil',
    '_method' => 'paginaPerfil'))); 

$rotas->add('atualizarPerfil', new Route('/atualizarPerfil', array('_controller' => 
    'SCE\Controllers\ControlePerfil',
    '_method' => 'atualizarPerfil'))); 
##FUNCÇÕES

#APONTAMENTO
$rotas->add('realizarExpedicao', new Route('/realizarExpedicao/{_param}', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'realizarExpedicao'))); 

$rotas->add('realizarRetorno', new Route('/realizarRetorno/{_param}', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'realizarRetorno'))); 


$rotas->add('apontar', new Route('/apontar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'apontar'))); 

$rotas->add('verificaCodigo', new Route('/verificaCodigo', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'verificaCodigo'))); 

//retirado o paramentro dessa rota para corrigir o problema de duplicação de apontamentos dia 28/28/2018 as 14:20
$rotas->add('apontarPalete', new Route('/apontarPalete', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'apontarPalete'))); 

$rotas->add('realizarApontamento', new Route('/realizarApontamento', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'realizarApontamento'))); 

$rotas->add('editarApontamento', new Route('/editarApontamento', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'editarApontamento'))); 

$rotas->add('reapontar', new Route('/reapontar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'reapontar'))); 

$rotas->add('verificaCodigoEditar', new Route('/verificaCodigoEditar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'verificaCodigoEditar'))); 

$rotas->add('verificaCodigoReapontar', new Route('/verificaCodigoReapontar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'verificaCodigoReapontar'))); 

$rotas->add('reapontarFinal', new Route('/reapontarFinal', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'reapontarFinal'))); 

$rotas->add('salvarReapontar', new Route('/salvarReapontar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'salvarReapontar'))); 


#ENDEREÇAMENTO
$rotas->add('enderecar', new Route('/enderecar', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'enderecar'))); 

$rotas->add('verificaPaleteEnderecar', new Route('/verificaPaleteEnderecar', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'verificaPaleteEnderecar'))); 

$rotas->add('enderecarPalete', new Route('/enderecarPalete', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'enderecarPalete'))); 

$rotas->add('realizarEnderecamento', new Route('/realizarEnderecamento', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'realizarEnderecamento'))); 

##REENDEREÇAMENTO

$rotas->add('editarEnderecamento', new Route('/editarEnderecamento', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'editarEnderecamento'))); 


$rotas->add('realizarEnderecamentoEditar', new Route('/realizarEnderecamentoEditar', array('_controller' => 
    'SCE\Controllers\ControleEnderecamento',
    '_method' => 'realizarEnderecamentoEditar'))); 

#EXPEDIÇÃO
$rotas->add('expedir', new Route('/expedir', array('_controller' => 
    'SCE\Controllers\ControleExpedicao',
    '_method' => 'expedir'))); 

$rotas->add('verificaCodigoOrdem', new Route('/verificaCodigoOrdem', array('_controller' => 
    'SCE\Controllers\ControleExpedicao',
    '_method' => 'verificaCodigoOrdem'))); 

$rotas->add('expedirPalete', new Route('/expedirPalete', array('_controller' => 
    'SCE\Controllers\ControleExpedicao',
    '_method' => 'expedirPalete'))); 

$rotas->add('verificaExpedirPalete', new Route('/verificaExpedirPalete', array('_controller' => 
    'SCE\Controllers\ControleExpedicao',
    '_method' => 'verificaExpedirPalete'))); 


#RELATÓRIOS
$rotas->add('relatorios', new Route('/relatorios', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorios'))); 

$rotas->add('relatorioPaleteEnderecado', new Route('/relatorioPaleteEnderecado', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaleteEnderecado'))); 

$rotas->add('relatorioPaleteApontado', new Route('/relatorioPaleteApontado', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaleteApontado'))); 

$rotas->add('relatorioPaleteApontadoGenerico', new Route('/relatorioPaleteApontadoGenerico', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaleteApontadoGenerico'))); 

$rotas->add('relatorioExpedicao', new Route('/relatorioExpedicao', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioExpedicao'))); 

$rotas->add('relatorioExpedicaoGenerico', new Route('/relatorioExpedicaoGenerico', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioExpedicaoGenerico'))); 

$rotas->add('relatorioPaleteGenericoEnderecado', new Route('/relatorioPaleteGenericoEnderecado', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaleteGenericoEnderecado'))); 

$rotas->add('relatorioPaleteEnderecadoGenerico', new Route('/relatorioPaleteEnderecadoGenerico', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaleteEnderecadoGenerico'))); 

$rotas->add('relatorioRetornoProcesso', new Route('/relatorioRetornoProcesso', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioRetornoProcesso'))); 

$rotas->add('relatorioTodosApontamentos', new Route('/relatorioTodosApontamentos', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioTodosApontamentos'))); 

##CONTROLE RETORNO DE PROCESSO


$rotas->add('controleRetorno', new Route('/controleRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'controleRetorno'))); 

$rotas->add('cadastrarOrdemRetorno', new Route('/cadastrarOrdemRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'cadastrarOrdemRetorno'))); 

$rotas->add('editarOrdemRetorno', new Route('/editarOrdemRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'editarOrdemRetorno'))); 

$rotas->add('editarOrdemRetorno', new Route('/editarOrdemRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'editarOrdemRetorno'))); 

$rotas->add('paginaRetorno', new Route('/paginaRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'paginaRetorno'))); 

$rotas->add('verificaCodigoOrdemRetorno', new Route('/verificaCodigoOrdemRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'verificaCodigoOrdemRetorno'))); 

$rotas->add('retornarPalete', new Route('/retornarPalete', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'retornarPalete'))); 

$rotas->add('verificaExpedirPaleteRetorno', new Route('/verificaExpedirPaleteRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'verificaExpedirPaleteRetorno'))); 

$rotas->add('finalizarOrdemRetorno', new Route('/finalizarOrdemRetorno', array('_controller' => 
    'SCE\Controllers\ControleRetorno',
    '_method' => 'finalizarOrdemRetorno'))); 


##CONTROLE REATIVAMENTO
$rotas->add('reativamento', new Route('/reativamento', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'reativamento'))); 

$rotas->add('reativar', new Route('/reativar', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'reativar'))); 

$rotas->add('relatorioPaletePorExpedicao', new Route('/relatorioPaletePorExpedicao', array('_controller' => 
    'SCE\Controllers\ControleRelatorio',
    '_method' => 'relatorioPaletePorExpedicao'))); 


##ROTAS DE TESTE DA API PARA O NOVO WMS
$rotas->add('apiApontamento', new Route('/apiApontamento', array('_controller' => 
    'SCE\Controllers\ControleApi',
    '_method' => 'apiApontamento'))); 

##RETORNO
$rotas->add('retorno', new Route('/retorno', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'retorno'))); 

$rotas->add('confirmarExpedicao', new Route('/confirmarExpedicao', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'confirmarExpedicao'))); 

$rotas->add('confirmarRetorno', new Route('/confirmarRetorno', array('_controller' => 
    'SCE\Controllers\ControleApontamento',
    '_method' => 'confirmarRetorno'))); 


return $rotas;
