var expedir = [];

$(document).ready(function () {
    $('#formConfirmarExpedir').submit(function () {
        var dados = $('#valoresExpedir').val();
        $("#confirmarExpedicao").modal().hide();
        $.ajax({
            url: "confirmarExpedicao",
            type: "post",
            data: { dados: dados },
            success: function (result) {
                if (result == 1) {
                    $("#sucesso").modal();

                } else {
                    if (result == 0) {
                        $("#falha").modal();
                    }
                }
            },
            error: function () {
                $("#falha").modal();
            }
        });
        return false;
    });
});


$(document).ready(function () {
    $('#formExpedir').submit(function () {
        var codigo = $('#codigo').val();
        $("#valoresExpedir").val(null);
        $("#botaoExpedir").prop("disabled", "disabled");

        $.getJSON('http://localhost/sce/public_html/realizarExpedicao/' + codigo, function (data) {
            console.log(data);
            var element = document.getElementById("corpoFormulario");
            element.innerHTML = '';
            var tm = data.length;
            if (tm === 0) {
                element.innerHTML = element.innerHTML
                    + '<div class="alert alert-danger" role="alert">Não foi encontrado nenhum material com o código digitado!<br><br> CÓDIGO DIGITADO: ' + codigo + '</div>';
            } else {
                element.innerHTML = element.innerHTML + ' <label for="nomeDesativar">Selecione os materiais que deseja expedir!</label>';
                $.each(data, function (key, item) {
                    if (item.saldo === "2") {
                        //siginifica que o material já foi expedido alguma vez
                        element.innerHTML = element.innerHTML + '<div class="input-group" style="margin-bottom:5px">'
                            + '<span class="input-group-addon">'
                            + '<i class="fa fa-circle fa-lg" aria-hidden="true" style="color: #D2691E"></i>                    '
                            + '</span>'
                            + '<span class="input-group-addon">'
                            + '<input type="checkbox" aria-label="..." value="' + item.idApontamento + '" onchange="verificarExpedir(' + item.idApontamento + ')">'
                            + '</span>'
                            + '<input type="text" class="form-control" disabled="" aria-label="..."  id="COD' + item.idApontamento + '" value="' + item.codigo + '" >'
                            + '</div>';
                    } else {

                        element.innerHTML = element.innerHTML + '<div class="input-group" style="margin-bottom:5px">'
                            + '<span class="input-group-addon">'
                            + '<i class="fa fa-circle fa-lg" aria-hidden="true" style="color: #228B22"></i>                    '
                            + '</span>'
                            + '<span class="input-group-addon">'
                            + '<input type="checkbox" aria-label="..." value="' + item.idApontamento + '" onchange="verificarExpedir(' + item.idApontamento + ')">'
                            + '</span>'
                            + '<input type="text" class="form-control" disabled="" aria-label="..."  id="COD' + item.idApontamento + '" value="' + item.codigo + '" >'
                            + '</div>';
                    }
                });
            }
            $("#confirmarExpedicao").modal();

        });

        return false;

    });
});


function verificarExpedir(id) {
    var pos = expedir.indexOf(id);
    if (pos == -1) {
        //adiciona no array
        expedir.push(id);
        $('#COD' + id).prop('readonly', false);
    } else {
        //removo do array
        expedir.splice(pos, 1)
        $('#COD' + id).prop('readonly', true);
    }
    $("#valoresExpedir").val(expedir);
    var tam = expedir.length;

    if (tam >= 1) {
        $("#botaoExpedir").prop("disabled", "");
    } else {
        $("#botaoExpedir").prop("disabled", "disabled");
    }
}
