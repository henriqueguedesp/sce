$(document).ready(function () {
    $('#formRetorno').submit(function () {
        const data = $('#formRetorno').serialize();
        $.ajax({//Função AJAX
            url: "realizarRetorno",
            type: "post",
            data,
            success: function (result) {
                switch (result) {
                    case '0':
                        $('#confirmarRetorno').hide();

                        $('#modalSucesso').modal();
                        break;
                    case '1':
                        $('#confirmarRetorno').hide();

                        $('#modalApontamentoExistente').modal();
                        break;
                    default:
                        $('#confirmarRetorno').hide();
                        $('#modalFalhaSalvarApontamento').modal();
                        break;
                }            
        },
            error: function () {
                alert('Erro 664!');
            }
        });
    return false;
});
});


function retornar(id, codigo) {
    $("#confirmarRetorno").modal();
    document.getElementById('idApontamento').value = id;
    document.getElementById('codigo').value = codigo;

}
