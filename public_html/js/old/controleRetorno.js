
function editar(ordem, lote, id) {
    jQuery.noConflict();

    $("#editarOrdem").modal();

    document.getElementById('codigoOrdemEditar').value = ordem;
    document.getElementById('numeroLoteEditar').value = lote;
    document.getElementById('id').value = id;
}

$(document).ready(function () {
    $('#formEditar').submit(function () {
        var codigoOrdem = $('#codigoOrdemEditar').val();
        var numeroLote = $('#numeroLoteEditar').val();
        var id = $('#id').val();
        $.ajax({//Função AJAX
            url: "editarOrdemRetorno",
            type: "post",
            data: {codigoOrdem: codigoOrdem, numeroLote: numeroLote,id:id},
            success: function (result) {
                if (result == 0) {
                    $("#editarOrdem").hide();
                    jQuery.noConflict();
                    $("#sucessoEditar").modal();

                } else {
                    if (result == 1) {
                        $("#avisoEditar").show();
                        $("#avisoEditar").removeClass(' alert alert-success');
                        $("#avisoEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código de Ordem de Retorno já cadastrada!</center>");
                        document.getElementById('codigoOrdemEditar').style.border = "2px solid #FF6347";
                    }
                    if (result == 2) {
                        $("#avisoEditar").show();
                        $("#avisoEditar").removeClass(' alert alert-success');
                        $("#avisoEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código de Ordem de Retorno inválido!</center>");
                        document.getElementById('codigoOrdem').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

$(document).ready(function () {
    $('#form').submit(function () {
        var codigoOrdem = $('#codigoOrdem').val();
        var numeroLote = $('#numeroLote').val();
        $.ajax({//Função AJAX
            url: "cadastrarOrdemRetorno",
            type: "post",
            data: {codigoOrdem: codigoOrdem, numeroLote: numeroLote},
            success: function (result) {
                if (result == 0) {
                    $("#cadastrarOrdem").hide();
                    jQuery.noConflict();
                    $("#sucesso").modal();

                } else {
                    if (result == 1) {
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código de Ordem de Retorno já cadastrada!</center>");
                        document.getElementById('codigoOrdem').style.border = "2px solid #FF6347";
                    }
                    if (result == 2) {
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código de Ordem de Retorno inválido!</center>");
                        document.getElementById('codigoOrdem').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

