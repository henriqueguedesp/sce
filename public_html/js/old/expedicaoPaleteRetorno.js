$(document).ready(function () {

    $('#form').submit(function () {
        var ordem = $('#codigoOrdem').val();
        var palete = $('#codigoPalete').val();
        var nota = $('#nota').val();
        $.ajax({//Função AJAX
            url: "verificaExpedirPaleteRetorno",
            type: "post",
            data: {codigoOrdem: ordem, codigoPalete: palete, nota: nota},
            success: function (result) {
                if (result == 0) {
                    jQuery.noConflict();
                    $("#sucesso").modal();

                } else {
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O material do não está em BAG!</center>");
                    }
                    if (result == 11) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O lote do BAG é diferente do lote cadastrado na Ordem de Retorno!</center>");
                    }
                    if (result == 12) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O BAG não está apontado ou já foi expedido!</center>");
                    }
                    if (result == 13) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O BAG não está endereçado !</center>");
                    }
                    if (result == 30) {
                        jQuery.noConflict();
                        document.getElementById('codigoOrdemFinalizar').value = ordem;
                        document.getElementById("quantidade").focus();
                        $("#modalFinalizarOrdem").modal();
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
