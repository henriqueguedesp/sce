//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {

    $('#form').submit(function () {
        var posicao = $('#codigoPosicao').val();
        var palete = $('#codigoPalete').val();
        $.ajax({//Função AJAX
            url: "realizarEnderecamento",
            type: "post",
            data: {codigoPalete: palete, codigoPosicao: posicao},
            success: function (result) {
                if (result == 0) {
                    //location.href = "/sga/public_html/enderecarPalete/"+codigo;
                    alert('Palete endereçado com sucesso!');
                    location.href = '/sga/public_html/';



                } else {
                    if (result == 1) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código já está endereçado! Verifique por favor!</center>");
                    }
                    if (result == 3) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código não existente ou não está apontado! Verifique por favor!</center>");
                    }
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código de posição inválido!</center>");
                    }
                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
