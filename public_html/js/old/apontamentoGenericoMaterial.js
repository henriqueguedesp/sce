//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var destino = $('#codigoDestino').val();
        var quantidade = $('#quantidade').val();
        var origem = $('#codigoOrigem').val();
        $.ajax({//Função AJAX
            url: "verificaPaleteDestino",
            type: "post",
            data: {origem: origem, destino: destino, quantidade: quantidade},
            success: function (result) {
                if (result == 1) {
                    alert("Apontamento realizado com sucesso!");
                    location.href = "/sga/public_html/";
                  

                } else {                    
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! O código digitado não corresponde a um palete genérico!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
