$(document).ready(function () {

    $('#form').submit(function () {
        var ordem = $('#codigoOrdem').val();
        var palete = $('#codigoPalete').val();
        var nota = $('#nota').val();
        $.ajax({//Função AJAX
            url: "verificaExpedirPalete",
            type: "post",
            data: {codigoOrdem: ordem, codigoPalete: palete,nota:nota},
            success: function (result) {
                if (result == 1) {
                    // location.href = "/sga/public_html/expedirPalete/" + codigo;
                    alert("Expedição realizada com sucesso!");
                    location.href = "/sga/public_html/";

                } else {
                    if (result == 10) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Código do palete inválido!</center>");
                    }
                    if (result == 30) {
                        jQuery.noConflict();
                        document.getElementById('codigoOrdemFinalizar').value = ordem;
                          document.getElementById("pesoOrdem").focus();
                        $("#modalFinalizarOrdem").modal();

                    }
                    if (result == 0) {
                        $("#aviso").hide();
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Palete não endereçado ou código inexistente!</center>");
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
