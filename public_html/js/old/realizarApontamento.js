//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#form').submit(function () {
        var codigoPalete = $('#codigoPalete').val();
        var quantidade = $('#quantidade').val();
        if (quantidade > 0) {
            jQuery.noConflict();
        }

        $('#modalSalvarApontamento').modal();
        $.ajax({//Função AJAX
            url: "realizarApontamento",
            type: "post",
            data: {codigoPalete: codigoPalete, quantidade: quantidade},
            success: function (result) {

                switch (result) {
                    case '0':
                        $('#modalSalvarApontamento').hide();
                        $('#modalSucessoSalvarApontamento').modal('show');
                        break;
                    case '1':
                        $('#modalSalvarApontamento').hide();

                        $('#modalApontamentoExistente').modal('show');
                        break;
                    default:
                        $('#modalSalvarApontamento').hide();
                        $('#modalFalhaSalvarApontamento').modal('show');
                        break;
                }

            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;


    });
});
