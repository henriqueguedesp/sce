var retornar = [];

$(document).ready(function () {
    $('#formConfirmarExpedir').submit(function () {
        //var dados = $('#valoresRetornar').val();
        const data = $('#formConfirmarExpedir').serialize();

        $("#confirmarExpedicao").modal().hide();
        $.ajax({
            url: "confirmarRetorno",
            type: "post",
            data,
            success: function (result) {
                if (result == 1) {
                    $("#sucesso").modal();

                } else {
                    if (result == 0) {
                        $("#falha").modal();
                    }
                }
            },
            error: function () {
                $("#falha").modal();
            }
        });
        return false;
    });
});


$(document).ready(function () {
    $('#formExpedir').submit(function () {
        var codigo = $('#codigo').val();
        $("#valoresRetornar").val(null);
        $("#botaoRetornar").prop("disabled", "disabled");

        $.getJSON('http://localhost/sce/public_html/realizarRetorno/' + codigo, function (data) {
            console.log(data);
            var element = document.getElementById("corpoFormulario");
            element.innerHTML = '';
            var tm = data.length;
            if (tm === 0) {
                element.innerHTML = element.innerHTML
                    + '<div class="alert alert-danger" role="alert">Não foi encontrado nenhum material com o código digitado!<br><br> CÓDIGO DIGITADO: ' + codigo + '</div>';
            } else {
                element.innerHTML = element.innerHTML + ' <label for="nomeDesativar">Selecione os materiais que deseja retornar!</label>';
                $.each(data, function (key, item) {
                    //siginifica que o material já foi expedido alguma vez
                    element.innerHTML = element.innerHTML + '<div class="input-group" style="margin-bottom:5px">'
                        + '<span class="input-group-addon">'
                        + '<input type="checkbox" aria-label="..." value="' + item.idApontamento + '" onchange="verificarExpedir(' + item.idApontamento + ')">'
                        + '</span>'
                        + '<input type="text" class="form-control" disabled="" id="COD' + item.idApontamento + '" value="' + item.codigo + '" >'
                        + '<select class="form-control"  disabled="true" name="SLT'+item.idApontamento+'" id="SLT'+item.idApontamento+'">'
                        + '<option class="form-control" value="0">Vazio</option>'
                        + '<option class="form-control" value="2">Com produto</option>'
                        + '</select>'
                        + '</div>';
                });
            }
            $("#confirmarExpedicao").modal();

        });

        return false;

    });
});


function verificarExpedir(id) {
    var pos = retornar.indexOf(id);
    if (pos == -1) {
        //adiciona no array
        retornar.push(id);
        $('#SLT' + id).prop('disabled', "");
    } else {
        //removo do array
        retornar.splice(pos, 1)
        $('#SLT' + id).prop('disabled', "disabled");
    }
    $("#valoresRetornar").val(retornar);
    var tam = retornar.length;

    if (tam >= 1) {
        $("#botaoRetornar").prop("disabled", "");
    } else {
        $("#botaoRetornar").prop("disabled", "disabled");
    }
}
